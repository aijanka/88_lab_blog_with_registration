const express = require('express');
const User = require('../models/User');

const createRouter = () => {
    const router = express.Router();

    router.post('/', (req, res) => {
        const user = new User(req.body);
        user.save()
            .then(user => res.send(user))
            .catch(error => res.status(400).send(error));
    });

    router.post('/sessions', async (req, res) => {
        const user = await User.findOne({username: req.body.username});
        if(!user) res.status(401).send('No such user');

        const isMatch = await user.checkPassword(req.body.password);
        if(!isMatch) res.send(404).send('Wrong password!');

        user.generateToken();
        await user.save();

        res.send({message: 'Username and password correct!', user});
    });

    router.delete('/sessions', async(req, res) => {
        const token = req.get('Token');
        const success = {message: "Successfully logged out!"};
        if(!token) res.send(success);

        const user = await User.findOne({token});
        if(!user) res.send(success);

        user.generateToken();
        user.save();
        return res.send(success);
    });



    return router;
};

module.exports = createRouter;