const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const ObjectId = require('mongodb').ObjectId;
const Post = require('../models/Post');
const config = require('../config');
const auth = require('../middlewares/auth');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

const createRouter = () => {
    const router = express.Router();

    router.get('/', (req, res) => {
        Post.find().populate('user')
            .then(posts => res.send(posts))
            .catch(error => res.sendStatus(404));
    });

    router.get('/:id', (req, res) => {
        Post.findOne({_id: req.params.id}).populate('user')
            .then(post => res.send(post))
            .catch(error => res.status(404).send(error, 'could not get the post'));
    })

    router.post('/', [auth, upload.single('image')], (req, res) => {
        const postData = req.body;
        if(req.file) {
            postData.image = req.file.filename;
        } else {
            postData.image = null;
        }

        const post = new Post(postData);
        post.save()
            .then(post => res.send(post))
            .catch(error => res.sendStatus(404));
    });

    return router;
};

module.exports = createRouter;