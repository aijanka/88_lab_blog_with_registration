const express = require('express');
const mongoose = require('mongoose');
const app = express();
const cors = require('cors');

const config = require('./config');
const users = require('./app/users');
const posts = require('./app/posts');

app.use(express.json());
app.use(cors());
app.use(express.static('public'));

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', () => {
    console.log('Mongoose connected');
    app.use('/users', users());
    app.use('/posts', posts());

    app.listen(8000, () => {
        console.log('connected to port 8000');
    });
});




