const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PostSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    description: String,
    image: String,
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    date: {
        type: String,
        default: (new Date).toUTCString()
    }
});

const Post = mongoose.model('Post', PostSchema);

module.exports = Post;