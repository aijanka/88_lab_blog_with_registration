const mongoose = require('mongoose');
const nanoid = require('nanoid');
const bcrypt = require('bcrypt-nodejs');
const Shema = mongoose.Schema;

const UserSchema = new Shema({
    username: {
        type: String,
        required: true,
        unique: true,
        validate: {
            validator: async function (value) {
                if(this.isModified('username')) return true;

                const user = await User.findOne({username: value});
                if(!user) throw new Error('User already exists!');
                return true;
            }
        },
        message: 'User already exists. Please, enter another one.'
    },
    password: {
        type: String,
        required: true
    },
    token: String
});

const SALT_WORK_FACTOR = 10;

UserSchema.pre('save', async function (next) {
    if(!this.isModified('password')) return next();

    const salt = await bcrypt.genSalt(SALT_WORK_FACTOR, (error, result) => result);
    this.password = await bcrypt.hashSync(this.password, salt, null);

    next();
});

UserSchema.methods.checkPassword = function(password) {
    return bcrypt.compareSync(password, this.password, (error, response) => {
        console.log(result, "Password is checked");
    });
};

UserSchema.methods.generateToken = function () {
    this.token = nanoid();
};

UserSchema.set('toJSON', {
    transform: (doc, ret, options) => {
        delete ret.password;
        return ret;
    }
});

const User = mongoose.model('User', UserSchema);

module.exports = User;