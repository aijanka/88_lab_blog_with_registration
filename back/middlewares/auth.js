const User = require('../models/User');

module.exports = async(req, res, next) => {
    const token = req.get('Token');
    if(!token) res.status(401).send('No token!');

    const user = await User.findOne({token});
    if(!user) res.status(403).send('No user found!');

    req.user = user;

    next();
};