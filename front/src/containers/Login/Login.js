import React, {Component} from 'react';
import {connect} from "react-redux";
import {login} from "../../store/actions/users";
import FormElement from "../../components/FormElement/FormElement";
import {Button, Col, Form, FormGroup} from "react-bootstrap";

class Register extends Component {

    state = {
        username: '',
        password: ''
    };

    inputChangeHandler = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    login = event => {
        event.preventDefault();
        this.props.loginUser(this.state);
    }

    render() {
        return (
            <Form horizontal>
                <FormElement
                    placeholder='Username'
                    type='text'
                    propertyName='username'
                    changeHandler={this.inputChangeHandler}
                    value={this.state.username}
                />

                <FormElement
                    placeholder='Password'
                    type='password'
                    propertyName='password'
                    changeHandler={this.inputChangeHandler}
                    value={this.state.password}
                />

                <FormGroup>
                    <Col smOffset={2} sm={10}>
                        <Button type="submit" onClick={this.login}>Log in</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => ({
    loginUser: (userData) => dispatch(login(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);
