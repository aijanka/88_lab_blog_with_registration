import axios from 'axios';
import {
    LOGIN_USER_FAILURE, LOGIN_USER_SUCCESS, LOGOUT_USER, REGISTER_USER_FAILURE,
    REGISTER_USER_SUCCESS
} from "./actionTypes";
import {NotificationManager} from 'react-notifications';
import {push} from 'react-router-redux';

const registerSuccess = user => ({type: REGISTER_USER_SUCCESS, user});
const registerFailure = error => ({type: REGISTER_USER_FAILURE, error});
export const register = (userData) => {
    return dispatch => {
        axios.post('/users', userData).then(response => {
            dispatch(registerSuccess(response.data.user));
            dispatch(push('/'));
            NotificationManager.success('Success', 'We congratulate you with successful registration! Now just login)');
        }, error => {
            dispatch(registerFailure(error.response.data));
            NotificationManager.error('Error', 'Some error with registering');
        })
    }
}


const loginSuccess = user => ({type: LOGIN_USER_SUCCESS, user});
const loginFailure = error => ({type: LOGIN_USER_FAILURE, error});
export const login = userData => {
    return dispatch => {
        axios.post('/users/sessions', userData).then(response => {
            dispatch(loginSuccess(response.data.user));
            dispatch(push('/'));
            NotificationManager.success('Success', 'You are successfully logged in)');
        }, error => {
            const errorObj = error.response ? error.response.data : {error: 'No internet'};
            dispatch(loginFailure(errorObj));
            NotificationManager.error('Error', 'Some error with logging in');
        });
    }
};

export const logout = () =>  {
    return dispatch => {
        axios.delete('/users/sessions').then(response => {
            dispatch({type: LOGOUT_USER});
            dispatch(push('/'));
            NotificationManager.success('Success', "Successfully logged out");
        }, error => {
            NotificationManager.error("Error", "Some error with logging out");
        })
    }
}
