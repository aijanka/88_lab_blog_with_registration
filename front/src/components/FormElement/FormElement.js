import React from 'react';
import {Col, ControlLabel, FormControl, FormGroup} from "react-bootstrap";

const FormElement = props => (
    <FormGroup>
        <Col componentClass={ControlLabel} sm={2}>{props.placeholder}</Col>
        <Col sm={9}>
            <FormControl
                type={props.type}
                placeholder={props.placeholder}
                onChange={props.changeHandler}
                value={props.value}
                name={props.propertyName}
                required
            />
        </Col>
    </FormGroup>
);

export default FormElement;