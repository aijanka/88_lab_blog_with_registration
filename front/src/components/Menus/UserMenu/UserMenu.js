import React from 'react';
import {Nav, NavItem} from "react-bootstrap";

const UserMenu = ({user, logout}) => {
    console.log(user);
    return (
        <Nav pullRight>
            <NavItem>Hello, {user.username} </NavItem>
            <NavItem onClick={logout}>Logout</NavItem>
        </Nav>
    );
}

export default UserMenu;