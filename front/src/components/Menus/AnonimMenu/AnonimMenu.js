import React from 'react';
import {Nav, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

const AnonimMenu = props => (
    <Nav pullRight>
        <Nav>
            <LinkContainer to='/register' exact>
                <NavItem>Register</NavItem>
            </LinkContainer>
        </Nav>
        <Nav>
            <LinkContainer to='/login' exact>
                <NavItem>Login</NavItem>
            </LinkContainer>
        </Nav>
    </Nav>
);

export default AnonimMenu;