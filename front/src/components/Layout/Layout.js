import React from 'react';
import Toolbar from "../Toolbar/Toolbar";
import {connect} from "react-redux";
import {logout} from "../../store/actions/users";

const Layout = props => (
    <div>
        <Toolbar
            user={props.user}
            logout={props.logoutUser}
        />
        {props.children}
    </div>
);

const mapStateToProps = state => ({
    user: state.user.user,
});

const mapDispatchToProps = dispatch => ({
    logoutUser: () => dispatch(logout())
})

export default connect(mapStateToProps, mapDispatchToProps)(Layout);