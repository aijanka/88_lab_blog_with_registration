import React from 'react';
import {Navbar} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import UserMenu from "../Menus/UserMenu/UserMenu";
import AnonimMenu from "../Menus/AnonimMenu/AnonimMenu";

const Toolbar = ({user, logout}) => (
    <Navbar>
        <Navbar.Header>
            <Navbar.Brand>
               <LinkContainer to='/'><a>Forum</a></LinkContainer>
            </Navbar.Brand>
        </Navbar.Header>

        {user ? <UserMenu user={user} logout={logout}/> : <AnonimMenu/>}
    </Navbar>
);

export default Toolbar;