import React, { Component } from 'react';
import Layout from "./components/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";

class App extends Component {
  render() {
    return (
        <Layout>
            <Switch>
                {/*<Route path='/' exact component={Posts}/>*/}
                <Route path='/register' exact component={Register}/>
                <Route path='/login' exact component={Login}/>
            </Switch>
        </Layout>
    );
  }
}

export default App;
